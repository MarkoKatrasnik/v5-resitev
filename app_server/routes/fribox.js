var express = require('express');
var router = express.Router();
var ctrlFribox = require('../controllers/fribox');

/* GET fribox page. */
router.get('/', ctrlFribox.fribox);
router.post('/', ctrlFribox.upload);
router.post('/delete', ctrlFribox.delete)

module.exports = router;
