var mongoose = require('mongoose');

var filesSchema = new mongoose.Schema({
  name: {type: String, unique : true, required: true},
  size: {type: Number, required: true},
  mimeType: {type: String, required: true},
  data: {type: Buffer, required: true}
});

mongoose.model('File', filesSchema, 'Files');